const express = require('express');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
      title: 'IMDB Rest API',
      version: '1.0.0',
      description:
        'Rest API made with Typescript, Express, Typeorm and others',
      license: {
        name: 'Licensed Under MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
      contact: {
        name: 'Thrashattack',
        url: 'https://github.com/thrashattack',
      },
    },
    servers: [
      {
        url: 'http://localhost:8080',
        description: 'REST API',
      },
    ],
};
  
var app = express();

const swaggerSpec = swaggerJSDoc({
    swaggerDefinition,
    apis: ["docs/endpoints/*.yml"],
});

app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.listen(9000, () => {
    console.log('[SWAGGER UI] DOCS AT http://localhost:9000/');
});