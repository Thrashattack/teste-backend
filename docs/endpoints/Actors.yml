openapi: 3.0.0

info:
  title: IMDB Rest API
  description: Provides endpoints for managing Users, Movies and Actors of the platform. 
  version: 1.0.0

servers:
  - url: http://localhost/imdb/api/v1
    description: Local development server

components:
  securitySchemes:
    bearerAuth:            
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    Actor:
      properties:
        name:
          type: string
          example: 'Chuck Norris'            
        age:
          type: integer
          example: 33            
        oscars:
          type: integer
          example: NaN            
        bio:
          type: string
          example: 'Chuck Norris bio can be writted using only the words from his name.'
      required:  
        - age
        - name
        - oscars
        - bio
    Response:
      properties:
        code:
          type: string
          example: '200'
        msg:
          type: string
          example: '[REST API] Succesful'
        instructions:
          type: string[]
          example: []
        response:
          type: object
          example:
            schema:
              $ref: '#/components/schemas/Actor'
              properties:
                id:
                  type: integer
                  example: 1
                created:
                  type: string
                  example: '2020-31-12 13:00:00 GMT -4'
                modified:
                  type: string
                  example: '2020-31-12 13:00:00 GMT -4'
                deleted:
                  type: string
                  example: 'null'
        tokenRequest:
          type: string
          example: 'ab98-22bb-ff21-121f-939f'
        execution:
          type: string
          example: '2020-31-12 13:00:00 GMT -4'
  responses:
    Unauthorized:
      description: Access token is missing, invalid or session is expired.
    BadRequest:
      description: Some of the parameters in body, path or query are missing, bad formated or maybe your auth token belongs to a unknow user.
    Forbidden:
      description: You need to be an Administrator to access this resource.
    InternalServerError:
      description: Something strange is happening here. We are already working to fix it.

security:
  - bearerAuth: []

paths:
  /actors:
    post:
      summary: Creates a new Actor
      description: Verifies for the body fields and creates a new Actor. Returns it in response or a representative Error.
      security: 
        - bearerAuth: [admin]
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Actor'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Response'
        '400':
          $ref: '#/components/responses/BadRequest' 
        '401':
          $ref: '#/components/responses/Unauthorized' 
        '403':
          $ref: '#/components/responses/Forbidden' 
        '500':   
          $ref: '#/components/responses/InternalServerError' 
  /actors/{id}:
    put:
      summary: Edit the actor for the given id
      description: Verifies for the body fields and update the Actor. Returns it in response or a representative Error.
      security: 
        - bearerAuth: [admin]
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Actor'
      parameters:
        - in: path
          id: actorId
          required: true
          type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Response'
        '400':
          $ref: '#/components/responses/BadRequest' 
        '401':
          $ref: '#/components/responses/Unauthorized' 
        '403':
          $ref: '#/components/responses/Forbidden' 
        '500':   
          $ref: '#/components/responses/InternalServerError' 
    delete:
      summary: Delete the actor for the given id
      description: Verifies for the body fields and delete the Actor. Returns it in response or a representative Error.
      security: 
        - bearerAuth: [admin]      
      parameters:
        - in: path
          id: actorId
          required: true
          type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Response'
        '400':
          $ref: '#/components/responses/BadRequest' 
        '401':
          $ref: '#/components/responses/Unauthorized' 
        '403':
          $ref: '#/components/responses/Forbidden' 
        '500':   
          $ref: '#/components/responses/InternalServerError'