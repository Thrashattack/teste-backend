import { createConnection, getConnection } from 'typeorm';

import { Actors } from '@models/Actors';
import { Casts } from '@models/Casts';
import { Movies } from '@models/Movies';
import { Permissions } from '@models/Permissions';
import { Scores } from '@models/Scores';
import { Users } from '@models/Users';

import Logger from '@rest/RestLogger';
import Messages from '@rest/RestMessages';

/**
 * Database connection wrapper
 */
export default class Database {
  private static LOG: Logger = new Logger(__filename);

  /**
   * Build the connection to database, bind the models, and test it.
   */
  public static buildConnection = async () => {
    try {
      await createConnection({
        type: 'postgres',
        url: process.env.DATABASE_URL,
        entities: [Actors, Casts, Movies, Permissions, Scores, Users],
      });

      Database.LOG.info(Messages.DATABASE_OK(), ['PENDING']);

      const connection = getConnection();

      const queryRunner = connection.createQueryRunner();

      const now = await queryRunner.query('SELECT NOW();');

      Database.LOG.info(Messages.DATABASE_OK(), ['DONE', JSON.stringify(now)]);
    } catch (error) {
      Database.LOG.error(Messages.DATABASE_ERROR(), error);
    }
  };

  /**
   * Closes the connection
   */
  public static closeConnection = async () => {
    await getConnection().close();
  };
}
