import {
  Entity,
  Column,
  JoinTable,
  PrimaryGeneratedColumn,
  ManyToMany,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { scoresEnum } from './Enum';
import { Users } from './Users';
import { Movies } from './Movies';

@Entity()
export class Scores {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'integer' })
  @ManyToMany((type) => Users)
  @JoinTable()
  user: Users;

  @Column({ type: 'integer' })
  @ManyToMany((type) => Movies)
  movie: Movies;

  @Column({ enum: scoresEnum })
  score: scoresEnum;

  @CreateDateColumn({ name: 'createdAt' })
  created: string;

  @UpdateDateColumn({ name: 'modifiedAt' })
  modified: string;

  @DeleteDateColumn({ name: 'deletedAt' })
  deleted: string | null;
}
