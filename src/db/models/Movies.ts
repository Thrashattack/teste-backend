import {
  Entity,
  Column,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { genderEnum } from './Enum';
import { Casts } from './Casts';
import { Scores } from './Scores';

@Entity()
export class Movies {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ enum: genderEnum })
  gender: genderEnum;

  @Column()
  summary: string;

  @Column()
  duration: number;

  @CreateDateColumn({ name: 'createdAt' })
  created: string;

  @UpdateDateColumn({ name: 'modifiedAt' })
  modified: string;

  @DeleteDateColumn({ name: 'deletedAt' })
  deleted: string | null;

  @ManyToMany((type) => Casts)
  @JoinTable()
  casts: Casts[];

  @ManyToMany((type) => Scores)
  @JoinTable()
  scores: Scores[];
}
