import {
  Entity,
  Column,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Movies } from './Movies';
import { Actors } from './Actors';
import { roleEnum } from './Enum';

@Entity()
export class Casts {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'integer' })
  @ManyToMany((type) => Movies)
  movie: Movies;

  @Column({ type: 'integer' })
  @ManyToMany((type) => Actors)
  @JoinTable()
  starring: Actors;

  @Column({ enum: roleEnum })
  role: roleEnum;

  @CreateDateColumn({ name: 'createdAt' })
  created: string;

  @UpdateDateColumn({ name: 'modifiedAt' })
  modified: string;

  @DeleteDateColumn({ name: 'deletedAt' })
  deleted: string | null;
}
