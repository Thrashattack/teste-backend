import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, DeleteDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  username: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @CreateDateColumn({ name: 'createdAt' })
  created: string;

  @UpdateDateColumn({ name: 'modifiedAt' })
  modified: string;

  @DeleteDateColumn({ name: 'deletedAt' })
  deleted: string | null;
}
