import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn } from 'typeorm';

@Entity()
export class Actors {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  age: number;

  @Column()
  oscars: number;

  @Column()
  bio: string;

  @CreateDateColumn({ name: 'createdAt' })
  created: string;

  @UpdateDateColumn({ name: 'modifiedAt' })
  modified: string;

  @DeleteDateColumn({ name: 'deletedAt' })
  deleted: string | null;
}
