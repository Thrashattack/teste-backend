import {
  Entity,
  Column,
  JoinTable,
  PrimaryGeneratedColumn,
  OneToOne,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { permissionLevelEnum } from './Enum';
import { Users } from './Users';

@Entity()
export class Permissions {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'integer' })
  @OneToOne((type) => Users)
  @JoinTable()
  user: Users;

  @Column({ enum: permissionLevelEnum })
  level: permissionLevelEnum;

  @CreateDateColumn({ name: 'createdAt' })
  created: string;

  @UpdateDateColumn({ name: 'modifiedAt' })
  modified: string;

  @DeleteDateColumn({ name: 'deletedAt' })
  deleted: string | null;
}
