enum roleEnum {
  DIRECTOR = 'director',
  ACTOR_OR_ACTRESS = 'actor/actress',
}

enum genderEnum {
  HORROR = 'horror',
  TERROR = 'terror',
  ROMANTIC = 'romantic',
  ADVENTURE = 'adventure',
  ACTION = 'action',
  ANIMATION = 'animation',
  DRAMA = 'drama',
}

enum permissionLevelEnum {
  ADMINISTRATOR = 'admin',
  USER = 'user',
}

enum scoresEnum {
  AWFUL = 0,
  BAD = 1,
  GOOD = 2,
  AWESOME = 3,
  WONDERFUL = 4,
}

export { roleEnum, genderEnum, permissionLevelEnum, scoresEnum };
