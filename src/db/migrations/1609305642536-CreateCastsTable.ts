import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateCastsTable1609305642536 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'casts',
        columns: [
          {
            name: 'id',
            type: 'serial',
            isPrimary: true,
          },
          {
            name: 'movie',
            type: 'serial',
            isNullable: false,
          },
          {
            name: 'starring',
            type: 'serial',
            isNullable: false,
          },
          {
            name: 'role',
            type: 'enum',
            enumName: 'rolesEnum',
            isNullable: false,
            enum: ['director', 'actor/actress'],
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
            isNullable: false,
          },
          {
            name: 'modifiedAt',
            type: 'timestamp',
            default: 'NOW()',
            isNullable: false,
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            isNullable: true,
            default: null,
          },
        ],
        foreignKeys: [
          {
            columnNames: ['movie'],
            referencedColumnNames: ['id'],
            referencedTableName: 'movies',
            onDelete: 'CASCADE',
          },
          {
            columnNames: ['starring'],
            referencedColumnNames: ['id'],
            referencedTableName: 'actors',
            onDelete: 'CASCADE',
          },
        ],
      }),
      true,
      true,
      true,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('casts', true, true, true);
  }
}
