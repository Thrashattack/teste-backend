import { MigrationInterface, QueryRunner } from 'typeorm';

import bcryptjs from 'bcryptjs';
import { permissionLevelEnum } from '../models/Enum';

export class CreateDefaultAdmin1610131233810 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('INSERT INTO users (id, name, username, email, password) VALUES ($1, $2, $3, $4, $5);', [
      1,
      'Administrator',
      'admin',
      'admin@admin.com',
      bcryptjs.hashSync('Admin@00', bcryptjs.genSaltSync(12)),
    ]);

    await queryRunner.query('INSERT INTO permissions ("user", level) VALUES ($1, $2)', [
      1,
      permissionLevelEnum.ADMINISTRATOR,
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DELETE FROM users WHERE name=$1 AND username=$2 AND email=$3 AND id=$4;', [
      'Administrator',
      'admin',
      'admin@admin.com',
      1,
    ]);

    await queryRunner.query('DELETE FROM permissions WHERE "user"=$1;', [1]);
  }
}
