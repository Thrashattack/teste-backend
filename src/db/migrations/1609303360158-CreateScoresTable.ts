import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateScoreTable1609303360158 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'scores',
        columns: [
          {
            name: 'id',
            type: 'serial',
            isPrimary: true,
          },
          {
            name: 'user',
            type: 'serial',
            isNullable: false,
          },
          {
            name: 'movie',
            type: 'serial',
            isNullable: false,
          },
          {
            name: 'score',
            type: 'enum',
            isNullable: false,
            enumName: 'scoresEnum',
            enum: ['0', '1', '2', '3', '4'],
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'NOW()',
            isNullable: false,
          },
          {
            name: 'modifiedAt',
            type: 'timestamp',
            default: 'NOW()',
            isNullable: false,
          },
          {
            name: 'deletedAt',
            type: 'timestamp',
            default: null,
            isNullable: true,
          },
        ],
        foreignKeys: [
          {
            columnNames: ['user'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: 'CASCADE',
          },
          {
            columnNames: ['movie'],
            referencedColumnNames: ['id'],
            referencedTableName: 'movies',
            onDelete: 'CASCADE',
          },
        ],
      }),
      true,
      true,
      true,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('scores', true, true, true);
  }
}
