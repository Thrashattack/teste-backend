import { MigrationInterface, QueryRunner } from 'typeorm';

export class ResetAdminAutoIncrement1610529698922 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('ALTER SEQUENCE users_id_seq RESTART WITH 2');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('ALTER SEQUENCE users_id_seq RESTART WITH 1');
  }
}
