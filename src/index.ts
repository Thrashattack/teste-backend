import('dotenv').then((dotenv) => {
  dotenv.config();
  import('./app').then((app) => new app.App().start().then());
});
