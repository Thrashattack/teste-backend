import RestRequest from '@rest/RestRequest';
import { Response } from 'express';

/**
 * Verifies if user is admin and returns a forbidden case not.
 */
export default function admin(req: RestRequest, res: Response, next: () => void) {
  const { isAdmin, bodyResponse } = req;
  if (!isAdmin) {
    bodyResponse.setError(403);
    return res.json(bodyResponse);
  }
  next();
}
