import { v1 } from 'uuid';
import { ExtractJwt } from 'passport-jwt';
import jwt from 'jwt-simple';
import { Response } from 'express';

import { permissionLevelEnum } from '@models/Enum';

import Logger from '@rest/RestLogger';
import ResponseDTO from '@rest/RestResponse';
import RestRequest from '@rest/RestRequest';

/**
 * Middlewares class to store callback middlewares
 */
export default class Middlewares {
  private LOG: Logger;

  constructor() {
    this.LOG = new Logger(__filename);
  }

  public getAll() {
    return [this.getTokenRequest, this.logInfo, this.createBodyResponse, this.setUserInfo.bind(this)];
  }
  /**
   * Generates a unique identifier for the request
   */
  public getTokenRequest(req: RestRequest, res: Response, next: () => void): void {
    req.tokenRequest = v1();
    next();
  }

  /**
   * Log information about the request
   */
  public logInfo = (req: RestRequest, res: Response, next: () => void): void => {
    this.LOG.info(`${req.tokenRequest}   ${req.method}        ==> ${req.path}`);
    this.LOG.info(
      `${req.tokenRequest}   Host         ${req.connection.remoteAddress.replace('::ffff:', '') || req.hostname}`,
    );
    this.LOG.info(`${req.tokenRequest}   Headers      ${JSON.stringify(req.headers).replace(/"/g, '')}`);
    this.LOG.info(`${req.tokenRequest}   Query        ${JSON.stringify(req.query).replace(/"/g, '')}`);
    this.LOG.info(`${req.tokenRequest}   Parameters   ${JSON.stringify(req.params).replace(/"/g, '')}`);
    this.LOG.info(`${req.tokenRequest}   Body         ${JSON.stringify(req.body)}`);
    next();
  };

  /**
   * Instantiate a new Response DTO in the request
   */
  public createBodyResponse(req: RestRequest, res: Response, next: () => void) {
    req.bodyResponse = new ResponseDTO();
    req.bodyResponse.setToken(req.tokenRequest);
    next();
  }

  /**
   * Set Request.isAdmin = true if the token payload have the permission level admin,
   * and false otherwise. Set user id on request.
   */
  public setUserInfo(req: RestRequest, res: Response, next: () => void) {
    try {
      const getToken = ExtractJwt.fromAuthHeaderAsBearerToken();

      const token = getToken(req);

      const authSecret = process.env.AUTH_SECRET;

      const userInfo = jwt.decode(token, authSecret);

      req.isAdmin = userInfo.permission === permissionLevelEnum.ADMINISTRATOR;

      req.user = userInfo.id;

      next();
    } catch (error) {
      this.LOG.info(
        `Creating User Info Failed for Request ${req.tokenRequest} - Headers : ${JSON.stringify(
          req.headers,
        )} | Error : ${error}`,
      );
      next();
    }
  }
}
