import { validationResult } from 'express-validator';

import Messages from '@rest/RestMessages';
import RestRequest from '@rest/RestRequest';
import { Response } from 'express';

/**
 * Validation Interceptor to send the appropriate message when validation errors occur.
 */
export default function intercept(req: RestRequest, res: Response, next: () => void) {
  const errors = validationResult(req);
  const instructionsMap = Object({
    required: Messages.IS_REQUIRED,
    email: Messages.INVALID_EMAIL,
    numeric: Messages.ONLY_NUMBERS,
    empty: Messages.EMPTY_VALUE,
    password: Messages.INVALID_FIELD,
  });

  if (!errors.isEmpty()) {
    const instructions: string[] = [];

    errors.array().forEach((item) => {
      const msg: string = item.msg;
      Object.keys(instructionsMap).includes(msg)
        ? instructions.push(instructionsMap[msg](item.param))
        : instructions.push(`${item.param} - ${item.msg}`);
    });
    req.bodyResponse.setInstructions(instructions);

    req.bodyResponse.setError(400);

    return res.json(req.bodyResponse);
  } else {
    next();
  }
}
