import passport from 'passport';
import passportJwt from 'passport-jwt';
import { getConnection } from 'typeorm';

import { Users } from '@models/Users';

const authSecret = process.env.AUTH_SECRET;

const { Strategy, ExtractJwt } = passportJwt;

const params = {
  secretOrKey: authSecret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};
/**
 * Generates a new passport Strategy to get the token from Auth Header as Bearer token and
 * loads it information based on the auth secret.
 * It will verify if the user data in token corresponds to a user in database and also verifies for
 * this user session expires date.
 */
const strategy = new Strategy(
  params,
  async (payload, done): Promise<void> => {
    try {
      const { id, email } = payload;

      const usersRepository = getConnection().getRepository(Users);

      const user = await usersRepository.findOneOrFail({
        where: { id, email },
      });

      done(null, user, payload);
    } catch (error) {
      console.log(error);
      return done('Error 401 - User or Token is invalid. Please signin again.', false);
    }
  },
);

passport.use(strategy);

/**
 * Default authenticate function to be bind to Route.all()
 */
export const authenticate = () => passport.authenticate('jwt', { session: false, authInfo: true });
