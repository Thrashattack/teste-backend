import { Router } from 'express';

import interceptor from '@middlewares/interceptor';

import admin from '@middlewares/admin';

import { authenticate } from '@middlewares/passaport';

import validators from './validators';

import services from './services';

export default Router()
  .all('/imdb/api/v1/actor/:id', authenticate())
  .all('/imdb/api/v1/actor', admin)
  .post('/imdb/api/v1/actor', validators.signUpValidator(), interceptor, services.newActor)
  .put('/imdb/api/v1/actor/:id', validators.editProfileValidator(), interceptor, services.editActor)
  .delete('/imdb/api/v1/actor/:id', validators.deleteProfileValidator(), interceptor, services.deleteActor);
