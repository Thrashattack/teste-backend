import { body, param } from 'express-validator';

const validations = {
  signUpValidator: () => [body(['name', 'age', 'oscars', 'bio'], 'required').notEmpty()],
  editProfileValidator: () => [body(['name', 'age', 'oscars', 'bio'], 'required').notEmpty()],
  deleteProfileValidator: () => [param('id', 'required').notEmpty().isNumeric()],
};
export default validations;
