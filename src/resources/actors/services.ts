import { Actors } from '@models/Actors';

import RestService from '@rest/RestService';
import { Response } from 'express';
import RestRequest from '@rest/RestRequest';

class Services extends RestService {
  constructor() {
    super(__filename);
  }

  public newActor = async (req: RestRequest, res: Response): Promise<Response> => {
    try {
      const { name, age, oscars, bio } = req.body;

      const payload = new Actors();

      payload.name = name;
      payload.age = age;
      payload.oscars = oscars;
      payload.bio = bio;

      try {
        const actor = await this.actorsRepository.insert(payload);

        return this.success(req, res, actor);
      } catch (error) {
        return this.badRequest(req, res, error);
      }
    } catch (error) {
      return this.internalServerError(req, res, error);
    }
  };

  public editActor = async (req: RestRequest, res: Response): Promise<Response> => {
    try {
      const { name, age, oscars, bio } = req.body;
      const { id } = req.params;

      const actor = new Actors();
      actor.name = name;
      actor.age = age;
      actor.oscars = oscars;
      actor.bio = bio;

      const updateResult = await this.actorsRepository.update(id, actor);

      return this.success(req, res, updateResult);
    } catch (error) {
      return this.internalServerError(req, res, error);
    }
  };

  public deleteActor = async (req: RestRequest, res: Response): Promise<Response> => {
    try {
      const { id } = req.params;

      const deleteResult = await this.actorsRepository.softDelete(id);

      return this.success(req, res, deleteResult);
    } catch (error) {
      return this.internalServerError(req, res, error);
    }
  };
}

export default new Services();
