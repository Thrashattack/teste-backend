import { body, param } from 'express-validator';

const validations = {
  signUpValidator: () => [
    body(['name', 'username', 'password', 'email'], 'required').notEmpty(),
    body('email', 'email').isEmail(),
    body('password', 'password').isStrongPassword({
      minLength: 8,
      minNumbers: 2,
      minSymbols: 2,
      minUppercase: 2,
    }),
  ],
  editProfileValidator: () => [
    body(['name', 'username'], 'required').notEmpty(),
    param('id', 'required').notEmpty().isNumeric(),
  ],
  deleteProfileValidator: () => [param('id', 'required').notEmpty().isNumeric()],
};
export default validations;
