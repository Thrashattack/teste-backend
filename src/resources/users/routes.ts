import { Router } from 'express';

import interceptor from '@middlewares/interceptor';

import { authenticate } from '@middlewares/passaport';

import admin from '@middlewares/admin';

import validators from './validators';

import services from './services';

export default Router()
  .post('/imdb/api/v1/user', validators.signUpValidator(), interceptor, services.signUpUser)
  .all('/imdb/api/v1/user/:id', authenticate())
  .put('/imdb/api/v1/user/:id', validators.editProfileValidator(), interceptor, services.editProfileUser)
  .delete('/imdb/api/v1/user/:id', admin, validators.deleteProfileValidator(), interceptor, services.deleteProfileUser);
