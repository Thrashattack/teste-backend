import { Response } from 'express';

import bcryptjs from 'bcryptjs';

import { Users } from '@models/Users';

import { Permissions } from '@models/Permissions';

import { permissionLevelEnum } from '@models/Enum';

import RestService from '@rest/RestService';
import RestRequest from '@rest/RestRequest';

class UsersService extends RestService {
  constructor() {
    super(__filename);
  }

  public signUpUser = async (req: RestRequest, res: Response): Promise<Response> => {
    try {
      const { name, username, password, email, admin } = req.body;

      const { isAdmin } = req;

      const newUser = new Users();

      newUser.name = name;
      newUser.username = username;
      newUser.password = bcryptjs.hashSync(password, bcryptjs.genSaltSync(12));
      newUser.email = email;
      try {
        const userInsertResult = await this.usersRepository.insert(newUser);

        const permission = new Permissions();

        permission.user = newUser;
        permission.level = isAdmin && admin ? permissionLevelEnum.ADMINISTRATOR : permissionLevelEnum.USER;

        const permissionInsertResult = await this.permissionsRepository.insert(permission);

        return this.success(req, res, { user: userInsertResult, permission: permissionInsertResult });
      } catch (error) {
        return this.badRequest(req, res, error);
      }
    } catch (error) {
      return this.internalServerError(req, res, error);
    }
  };

  public editProfileUser = async (req: RestRequest, res: Response): Promise<Response> => {
    try {
      const { name, username } = req.body;
      const { id } = req.params;

      const user = new Users();
      user.name = name;
      user.username = username;

      const updateResult = await this.usersRepository.update(id, user);

      return this.success(req, res, updateResult);
    } catch (error) {
      return this.internalServerError(req, res, error);
    }
  };

  public deleteProfileUser = async (req: RestRequest, res: Response): Promise<Response> => {
    try {
      const { id } = req.params;

      const userDeleteResult = await this.usersRepository.softDelete(id);

      const permissionDeleteResult = await this.permissionsRepository.softDelete(id);

      return this.success(req, res, { user: userDeleteResult, permissions: permissionDeleteResult });
    } catch (error) {
      return this.badRequest(req, res, error);
    }
  };
}

export default new UsersService();
