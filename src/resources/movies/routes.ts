import { Router } from 'express';

import interceptor from '@middlewares/interceptor';

import { authenticate } from '@middlewares/passaport';

import admin from '@middlewares/admin';

import validators from './validators';

import services from './services';

export default Router()
  .all('/imdb/api/v1/movies', authenticate())
  .post('/imdb/api/v1/movies', admin, validators.newMovieValidator(), interceptor, services.newMovie)
  .put('/imdb/api/v1/movies/:id', validators.voteMovieValidator(), interceptor, services.voteMovie)
  .get('/imdb/api/v1/movies', validators.listMoviesValidator(), interceptor, services.listMovies)
  .get('/imdb/api/v1/movies/:id', validators.movieInfoValidator(), interceptor, services.movieInfo);
