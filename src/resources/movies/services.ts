import { Like } from 'typeorm';

import { Users } from '@models/Users';
import { Actors } from '@models/Actors';
import { Casts } from '@models/Casts';
import { Movies } from '@models/Movies';
import { Scores } from '@models/Scores';

import { roleEnum } from '@models/Enum';
import RestService from '@rest/RestService';
import { Response } from 'express';
import RestRequest from '@rest/RestRequest';

class MoviesService extends RestService {
  constructor() {
    super(__filename);
  }

  private buildFilters = async (query: any, manager: any) => {
    const where: any = {};

    if (query.gender) {
      where.gender = Like(`%${query.gender}%`);
    }
    if (query.title) {
      where.title = Like(`%${query.title}%`);
    }
    if (query.director) {
      const actor = (await manager.findOneOrFail(Actors, query.director)) as Actors;
      const casts = (await manager.find(Casts, {
        where: { starring: actor, role: roleEnum.DIRECTOR },
      })) as Casts[];
      where.id = casts.map((cast: Casts) => cast.movie.id);
    }
    if (query.actor) {
      const actor = (await manager.findOneOrFail(Actors, query.actors)) as Actors;
      const casts = (await manager.find(Casts, {
        where: {
          starring: actor,
          role: roleEnum.ACTOR_OR_ACTRESS,
        },
      })) as Casts[];

      where.id = where.id
        ? [...where.id, casts.map((cast: Casts) => cast.movie.id)]
        : casts.map((cast: Casts) => cast.movie.id);
    }

    where.deleted = null;
    return where;
  };

  private scoreMedia = (scores: Scores[]) => {
    let scoreSum = 0;
    scores.forEach((score: Scores) => {
      scoreSum += score.score;
    });

    return scoreSum / scores.length;
  };

  public newMovie = async (req: RestRequest, res: Response): Promise<Response> => {
    try {
      const { title, gender, summary, duration, cast } = req.body;

      const payload = new Movies();

      payload.duration = duration;
      payload.gender = gender;
      payload.summary = summary;
      payload.title = title;

      const movie = await this.connectionManager.save(payload);

      cast.forEach(async (entry: { actor: number; role: any }) => {
        try {
          const starring = (await this.connectionManager.findOneOrFail(Actors, entry.actor, {
            where: {
              deleted: null,
            },
          })) as Actors;
          const newCast = new Casts();

          newCast.starring = starring;
          newCast.role = entry.role;
          newCast.movie = movie;

          await this.connectionManager.save(newCast);
        } catch (error) {
          return this.badRequest(req, res, error);
        }
      });

      return this.success(req, res, movie);
    } catch (error) {
      return this.internalServerError(req, res, error);
    }
  };

  public voteMovie = async (req: RestRequest, res: Response): Promise<Response> => {
    try {
      const { score } = req.body;
      const { id } = req.params;
      const { user } = req;

      const movie = (await this.connectionManager.findOneOrFail(Movies, id, {
        where: { deleted: null },
      })) as Movies;
      const voter = (await this.connectionManager.findOneOrFail(Users, user, {
        where: { deleted: null },
      })) as Users;

      const newScore = new Scores();

      newScore.movie = movie;
      newScore.user = voter;
      newScore.score = score;

      await this.connectionManager.save(newScore);

      return this.success(req, res, { movie, newScore });
    } catch (error) {
      return this.internalServerError(req, res, error);
    }
  };

  public listMovies = async (req: RestRequest, res: any): Promise<Response> => {
    try {
      const movies = (await this.connectionManager.find(Movies, {
        where: await this.buildFilters(req.query, this.connectionManager),
      })) as Movies[];

      return this.success(req, res, movies);
    } catch (error) {
      return this.internalServerError(req, res, error);
    }
  };

  public movieInfo = async (req: RestRequest, res: any): Promise<Response> => {
    try {
      const { id } = req.params;

      const movie = (await this.connectionManager.findOneOrFail(Movies, id, {
        where: { deleted: null },
      })) as Movies;

      const payload = {
        ...movie,
        averageScore: this.scoreMedia(movie.scores),
      };
      return this.success(req, res, payload);
    } catch (error) {
      return this.internalServerError(req, res, error);
    }
  };
}

export default new MoviesService();
