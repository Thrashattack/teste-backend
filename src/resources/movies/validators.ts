import { body, param } from 'express-validator';

const validations = {
  newMovieValidator: () => [
    body(['title', 'gender', 'summary', 'duration', 'cast', 'user'], 'required').notEmpty(),
    body(['duration', 'user'], 'invalid').isNumeric(),
    body('cast', 'invalid').isArray(),
  ],

  voteMovieValidator: () => [
    body(['user', 'score'], 'required').notEmpty(),
    body('score', 'invalid').isNumeric(),
    param('id', 'required').notEmpty().isNumeric(),
  ],

  listMoviesValidator: () => [],

  movieInfoValidator: () => [param('id', 'required').notEmpty().isNumeric()],
};
export default validations;
