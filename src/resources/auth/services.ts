import jwt from 'jwt-simple';

import bcrypt from 'bcryptjs';

import Messages from '@rest/RestMessages';

import RestService from '@rest/RestService';
import { Response } from 'express';
import RestRequest from '@rest/RestRequest';

class Auth extends RestService {
  private authSecret: string;

  constructor() {
    super(__filename);
    this.authSecret = process.env.AUTH_SECRET;
  }

  public signIn = async (req: RestRequest, res: Response): Promise<Response> => {
    try {
      const { email, password } = req.body;

      const user = await this.usersRepository.findOneOrFail({ where: { email } });

      if (!user) return this.badRequest(req, res, Messages.EMAIL_NOT_FOUND());

      const isMatch = bcrypt.compareSync(password, user.password);

      if (!isMatch) return this.unauthorized(req, res, Messages.INVALID_PASSWORD());

      const permission = await this.permissionsRepository.findOneOrFail({
        where: { user: user.id },
      });
      // Loggin OK!
      const expiresAt = Date.now() + 60 * 60 * 24 * 3; // 3 days

      const payload = {
        id: user.id,
        name: user.name,
        email: user.email,
        permission: permission.level,
        iat: Date.now(),
        exp: expiresAt,
      };

      const token = jwt.encode(payload, this.authSecret);

      return this.success(req, res, { login: Messages.LOGIN_OK(), token });
    } catch (error) {
      return this.internalServerError(req, res, error);
    }
  };
}

export default new Auth();
