import { body } from 'express-validator';

const validations = {
  signInValidator: () => [body(['password', 'email'], 'required').notEmpty(), body('email', 'email').isEmail()],
};
export default validations;
