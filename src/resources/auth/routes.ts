import { Router } from 'express';

import interceptor from '@middlewares/interceptor';

import validators from './validators';

import Auth from './services';

export default Router().post('/imdb/api/v1/signin', validators.signInValidator(), interceptor, Auth.signIn);
