// vendors
import express, { Express, json, Router } from 'express';
import { AddressInfo, Server } from 'net';
import cors from 'cors';
import 'reflect-metadata';

// db
import Database from '@db';

// middlewares
import Middlewares from '@middlewares/middlewares';

// routes
import UserRouter from '@resources/users/routes';
import MovieRouter from '@resources/movies/routes';
import ActorRouter from '@resources/actors/routes';
import AuthRouter from '@resources/auth/routes';

// utils
import Logger from '@rest/RestLogger';
import Messages from '@rest/RestMessages';

/**
 * API Entrypoint class. It will instantiate the express server, bind the routes and connect to the database.
 */
export class App {
  private app: Express;
  private server: Server;
  private address: string;
  private port: number;
  private LOG: Logger;

  /**
   * Bind the Routes and instantiate Express
   */
  constructor() {
    this.LOG = new Logger(__filename);

    this.app = express();

    this.port = Number(process.env.HOST_PORT);

    this.address = process.env.HOST_ADDRESS;
  }

  /**
   * Configure server routes
   *
   * @returns {void}
   */
  private configRoutes(): void {
    this.app.use(AuthRouter);

    this.app.use(UserRouter);

    this.app.use(ActorRouter);

    this.app.use(MovieRouter);
  }

  /**
   * Configure database connection
   *
   * @returns {Promise<void>}
   */
  private configDatabase = async (): Promise<void> => await Database.buildConnection();

  /**
   * Configure middlewares for Response DTO and metadatas
   *
   * @returns {void}
   */
  private configMiddlewares = (): void => {
    const middlewareBuilder = new Middlewares();
    this.app.use(json({ limit: '100MB' }), cors(), ...middlewareBuilder.getAll());
  };

  /**
   * Runner method, configure database and middlewares, also set up the server
   *
   * @returns {Promise<void>}
   */
  public start = async (): Promise<void> => {
    try {
      this.LOG.info(Messages.API_STARTING());

      await this.configDatabase();

      this.configMiddlewares();

      this.configRoutes();

      this.server = this.app.listen(this.port, this.address, () => {
        const { address, port } = this.server.address() as AddressInfo;

        const serverUrl = `http://${address}:${port}`;

        this.LOG.info(Messages.API_OK(), serverUrl);
      });
    } catch (error) {
      await Database.closeConnection();

      this.LOG.error(Messages.API_ERROR(), error);
    }
  };
}
