import Messages from '@rest/RestMessages';

/**
 * Response DTO to store default fields of response
 */
export default class Response {
  public code: string;
  public msg: string;
  public instructions: string[];
  public response: any;
  public tokenRequest: string;
  public execution: string;

  /**
   * @param code string
   * @param description string
   * @param instructions string[]
   * @param response any
   * @param tokenRequest string
   * @param execution timestamp
   */
  constructor(
    code: string = '0000',
    description: string = '',
    instructions: string[] = [],
    response: any = {},
    tokenRequest: string = '',
    execution: string = new Date().toLocaleString(),
  ) {
    this.code = code;
    this.msg = description;
    this.instructions = instructions;
    this.response = response;
    this.tokenRequest = tokenRequest;
    this.execution = execution;
  }

  public setStatus(code: number) {
    this.code = code.toString();
  }
  public setResponse(response: any) {
    this.response = response;
  }

  public setToken(tokenRequest: string) {
    this.tokenRequest = tokenRequest;
  }

  public setInstructions(instructions: string[]) {
    this.instructions = instructions;
  }

  public setError(code: number) {
    this.code = code.toString();
    switch (this.code) {
      case '400':
        this.msg =
          'Some of the parameters in body, path or query are missing, bad formated or maybe your auth token belongs to a unknow user.';
        break;
      case '401':
        this.msg = 'Access token is missing, invalid or session is expired.';
        break;
      case '403':
        this.msg = 'You need to be an Administrator to access this resource.';
        break;
      case '500':
        this.msg = 'Something strange is happening here. We are already working to fix it.';
        break;
      default:
        this.msg = 'Unknow Error!';
        break;
    }
    return this;
  }

  public setSuccess(response: any) {
    this.code = '200';
    this.msg = Messages.SUCCESS();
    this.response = response || {};
    return this;
  }
}
