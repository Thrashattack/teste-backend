import Response from '@rest/RestResponse';
import { Request } from 'express';

export default interface RestRequest extends Request {
  isAdmin: boolean;
  user: any;
  tokenRequest: string;
  bodyResponse: Response;
}
