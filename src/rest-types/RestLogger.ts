import log4js from 'log4js';

/**
 * Wrapper of Logger from log4js
 */
export default class Logger {
  private filename: string;
  private log: log4js.Logger;

  /**
   * @constructor
   * @param filename : string - Filename to append to logs
   */
  constructor(filename: string = __filename) {
    this.filename = filename;
    log4js.configure({
      appenders: { 'ionasys-imdb': { type: 'console' } },
      categories: { default: { appenders: ['ionasys-imdb'], level: 'info' } },
    });

    this.log = log4js.getLogger(' ');
  }

  public info(...message: any[]) {
    this.log.info(`[${this.replaceFileName()}]  ${message.join(' ')}`);
  }

  public warning(...message: any[]) {
    this.log.warn(`[${this.replaceFileName()}]  ${message.join(' ')}`);
  }

  public error(...message: any[]) {
    this.log.error(`[${this.replaceFileName()}]  ${message.join(' ')}`);
  }

  public logger() {
    return this.log;
  }

  public log4js() {
    return log4js;
  }

  public replaceFileName() {
    return this.filename.split('src/')[1] || this.filename.split('src/')[0];
  }
}
