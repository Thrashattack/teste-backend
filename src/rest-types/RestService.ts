import { EntityManager, getConnection, Repository } from 'typeorm';
import { Response } from 'express';

import Logger from '@rest/RestLogger';

import RestRequest from '@rest/RestRequest';

import { Actors } from '@models/Actors';
import { Casts } from '@models/Casts';
import { Movies } from '@models/Movies';
import { Permissions } from '@models/Permissions';
import { Scores } from '@models/Scores';
import { Users } from '@models/Users';

export default abstract class RestService {
  constructor(filename: string) {
    this.log = new Logger(filename);
  }

  public log: Logger;

  public connection = getConnection();

  public actorsRepository: Repository<Actors> = this.connection.getRepository(Actors);

  public castsRepository: Repository<Casts> = this.connection.getRepository(Casts);

  public moviesRepository: Repository<Movies> = this.connection.getRepository(Movies);

  public permissionsRepository: Repository<Permissions> = this.connection.getRepository(Permissions);

  public scoresRepository: Repository<Scores> = this.connection.getRepository(Scores);

  public usersRepository: Repository<Users> = this.connection.getRepository(Users);

  public connectionManager: EntityManager = this.connection.manager;

  public internalServerError = (req: RestRequest, res: Response, error: any): Response =>
    this.error(req, res, error, 500);

  public badRequest = (req: RestRequest, res: Response, error: any): Response => this.error(req, res, error, 400);

  public unauthorized = (req: RestRequest, res: Response, error: any): Response => this.error(req, res, error, 401);

  public forbidden = (req: RestRequest, res: Response, error: any): Response => this.error(req, res, error, 403);

  public success(req: RestRequest, res: Response, result: any): Response {
    req.bodyResponse.setStatus(200);

    req.bodyResponse.setSuccess(result);

    return res.json(req.bodyResponse);
  }

  private error(req: RestRequest, res: Response, error: any, code: number): Response {
    this.log.error(req.tokenRequest, error.stack || JSON.stringify(error));

    req.bodyResponse.setError(code);

    return res.json(req.bodyResponse);
  }
}
