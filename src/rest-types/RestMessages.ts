import Logger from '@rest/RestLogger';

const LOG = new Logger(__filename);

const body = (code: any, message: any, instructions: any, response: any) => {
  return {
    code,
    message,
    instructions: instructions || [],
    response: response || [],
  };
};
/**
 * Messages class to store default messages in API
 */
export default abstract class Messages {
  // Messages
  public static LOGIN_OK = () => 'Login Successful!';
  public static INVALID_PASSWORD = () => 'Wrong password!';
  public static EMAIL_NOT_FOUND = () => 'Email was not found. Please Sign Up.';
  public static MISSING_USER_OR_PASSWORD = () => 'User or password was not informed';
  public static UNKNOWN_ERROR = (message: string, response: any) =>
    body('[UNKNOWN_ERROR]', message || 'Unexpected error, please try again later.', null, response || null);
  public static SUCCESSFUL = (response: any) => body('[SUCCESSFUL]', 'Success!', null, response);
  public static INVALID_PARAMS = (arrayInstructions: string[], response: any) =>
    body('[INVALID_PARAMS]', 'Invalid parameters, please try again.', arrayInstructions || null, response || null);

  // Database
  public static DATABASE_OK = () => '[DATABASE] Database is up';
  public static DATABASE_ERROR = () => "[DATABASE] Failed to connect with database. API won't start.";
  public static NOT_FOUND_ENTITY = (entity: any) => `${entity} - not found.`;
  public static FAIL_SELECT = (field: any) => `[DATABASE] Can't search  ${field}, try again later.`;
  public static FAIL_INSERT = (field: any) => `[DATABASE] Can't create  ${field}, try again later.`;
  public static FAIL_UPDATE = (field: any) => `[DATABASE] Can't update ${field}, try again later.`;

  // Services
  public static SERVICE_FAIL = (response: any) =>
    body('[SERVICE]', 'Error while connecting with service.', ['try again later.'], [response]);
  public static SERVICE_ERROR = (name: string, response: any) =>
    body('[SERVICE]', `Error while executing the service ${name}.`, ['try again later.'], [response]);
  public static SERVICE_UNAVAILABLE = (response: any) =>
    body('[SERVICE]', 'Service is unavaliable. try again later.', null, [response]);
  public static FAIL_VALIDATE = (field: any) => `[SERVICE] ${field} is invalid.`;

  // API
  public static API_STARTING = () => '[REST API] Starting Rest API Server';
  public static API_OK = () => '[REST API] API IMDB OK! =]';
  public static SUCCESS = () => '[REST API] Operation Successful';
  public static API_ERROR = () => '[REST API] Error while trying to start the server';
  public static ERROR = () => '[REST API] Resource Error';
  public static IS_REQUIRED = (field: any) => `[REST API] ${field} - field is required.`;
  public static IS_INVALID = (field: any, valide: any) =>
    `[REST API] ${field} - field is invalid. (${JSON.stringify(valide) || valide || ''})`;
  public static HEADER_IS_REQUIRED = (field: any) => `[REST API] ${field} - header is required.`;
  public static EMPTY_VALUE = (field: any) => `[REST API] ${field} - field can't be empty.`;
  public static NOT_FOUND_USER = (field: any) => `[REST API] ${field} - client not found.`;
  public static INVALID_RANGE = (field: any, min: number, max: number) =>
    `[REST API] ${field} - invalid (min: ${min} - max: ${max}).`;
  public static INVALID_DATE = (field: any) => `[REST API] ${field} - date is invalid. ex: 2019-12-31`;
  public static INVALID_QUANTITY = (field: any, min: number) =>
    `[REST API] ${field} - invalid (greater or equals ${min}).`;
  public static INVALID_QUANTITY_LESS = (field: any, max: number) =>
    `[REST API] ${field} - invalid (lesser or equals ${max}).`;
  public static INVALID_EMAIL = (field: any) => `[REST API] ${field} - e-mail invalid.`;
  public static ONLY_NUMBERS = (field: any) => `[REST API] ${field} - alowed only numbers.`;
  public static INVALID_FIELD = (field: any) => `[REST API] ${field} - field is invalid.`;
  public static NOT_FOUND = (field: any) => `[REST API] ${field} - not found.`;
  public static INVALID_USER = () => '[REST API] Invalid user.';
  public static CLIENT_UUID_NOT_CURRENT_SESSION = () => "[REST API] cliente_uuid doesn't belong to the current session";
  public static INVALID_VALUE_BASE = () => `[REST API] Requested value differs from the database.`;

  // Logs
  public static logDebug = (component: any, stack: any, tokenRequest: string) => {
    LOG.warning(`[LOG] - logError - ${__filename}`);
    console.error(`[DEBUG] - ${tokenRequest} - ${component} ->`, stack);
  };
}
