# IMDB Rest API 


## Pré: 

- Antes de executar o projeto, renomeio o arquivo `.env.sample` para apenas `.env`. Este arquivo carrega as informações do ambiente.
- Em ambiente de produção essas informações devem ser cadastradas como variaveis de ambiente no processo de CI/CD.
- É necessário ter o docker instalado na máquina e que o usuário do docker esteja no grupo de sudoers.

## Executando: 

- Para executar o projeto basta rodar o comando `docker-compose up --build` no terminal. 
- A API será servida na porta 8080. Ex: `http://localhost:8080/`
- A database servida na porta 5432. Porém sobre o alias 'database' Ex: `http://database:5432/`
- O PGAdmin será servido na porta 16543. Ex: `http://localhost:16543/` 
    * Para o login no PGADMIN informe as credenciais : email "you@email.here", password: "yourpassword"
- Existe uma migration que cria um usuário administrador default com as credenciais: 
    * email: admin@admin.com
    * passowrd: Admin@00


## Descrição: 

- O Projeto foi feito utilizando o banco de dados Postgres, o Gerenciador PGAdmin e a linguagem Node.js. Tudo configurado no arquivo `docker-compose.yml`
- Também foi utilizado um ORM para gerenciar as conexões e queries SQL, o Typeorm. As configurações se encontram em `ormconfig.js` 
- O Projeto foi escrito utilizando Typescript. O Linter e as configurações do Typescript se encontram em `tslint.json` e `tsconfig.json` respectivamente.
- A arquitetura do projeto foi pensada com base no design Domain Drive Development (DDD) porém por questões de agilidade não foram implementadas as interfaces e injeção de dependencias. Contudo, a arquitetura está preparada pra essa refatoração. 
- A documentação foi feita utilizando JSdocs com BetterDocs. As configurações estão em `jsdoc.json`
- As coleções de testes foram feitas utilizando Postman e encontram-se no diretório `tests/`

## Documentação

- Para gerar a documentação utilize o script `docs` com `yarn docs` ou `npm run docs`. Isso irá iniciar o Swagger UI em `http://localhost:9000/`

## Entrypoint

- O ponto de entrada da API é a classe App em `src/app.ts`. Lá é realizada toda a configuração de ambiente, database, rotas, middlewares e do servidor em si. 

## DB:

- As migrations, as models e o script de conexão com o banco de dados encontram-se no diretório `src/db`

* Models: 
    - Actors : Entidade que representa os participantes de um filme
    - Casts : Entidade que representa a relação entre os atores e os filmes e especifica a função de determinado ator (Diretor, Ator/Atriz)
    - Enum : Enumeradores usados nas entidades
    - Movies : Entidade que representa os filmes com seu elenco de atores
    - Permissions: Entidade que mapeia a permissão de cada Usuário (Administrator, User)
    - Scores: Entidade que representa a relação entre um usuário e a nota dada por ele a um filme
    - Session: Entidade que representa a sessão de um usuário na API e armazena seu token de acesso
    - Users: Entidade que armazena os usuários da API

## Middlewares 

- Os interceptadores de validação, interceptadores de autenticação, interceptadores de permissão de usuário, geradores de log e injetores de DTO's estão aqui : `src/middlewares`

## Resources

- Os domínios da aplicação encontram-se em `src/resources` onde cada diretório representa um domínio contendo seus próprios recursos como Rotas, Servições e Validadores. 

## Utils 

- Foram criadas estruturas de auxilio de Loggin, Messaging e DTO's. Estas encontram-se em `src/utils`

## Scripts 

- Todos os scripts necessários para a utilização do projeto já estão sendo executados dentro do container docker. Porém aqui vai uma descrição de todos eles: 
    * prebuild : Exclue o diretorio `dist/` onde será transpilado o projeto e faz o lint.
    * build : Transpila o projeto e copia o package.json para dentro de `dist/`
    * prestart : Executa o comando build acima
    * start : Executa o entrypoint do projeto (dev)
    * migration:run : Executa o método up das migrations no diretorio `src/db/migrations`
    * migration:revert : Executa o método down das migrations no diretorio `src/db/migrations`
    * docs : Exclue o diretório `docs/` e gera os arquivos HTML de documentação novamente no diretório `docs/`