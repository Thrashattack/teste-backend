module.exports = {
    type: "postgres",
    host: "database",
    port: "5432",
    username: "postgres",
    password: "secret",
    database: "imdb",
    name: "default",
    synchronize: false,
    logging: true,
    migrationsTableName: "custom_migration_table",
    migrations: [
      "src/db/migrations/*.ts"
    ],
    cli: {
      migrationsDir: "src/db/migrations"
    },
  }